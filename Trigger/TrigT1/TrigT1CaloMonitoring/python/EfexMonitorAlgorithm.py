#
#  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
#
def EfexMonitoringConfig(inputFlags):
    '''Function to configure LVL1 Efex monitoring algorithm'''

    # get the component factory - used for merging the algorithm results
    from AthenaConfiguration.ComponentFactory import CompFactory
    from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
    result = ComponentAccumulator()

    # add algorithm to the helper
    result.addEventAlgo( CompFactory.EfexMonitorAlgorithm('EfexMonAlg',
                                                  PackageName='EfexMonitor',
                                                  LowPtCut = 0.0,
                                                  HiPtCut = 15000.0,
                                                  eFexEMTobKeyList = ['L1_eEMRoI', 'L1_eEMxRoI'],
                                                  eFexTauTobKeyList = ['L1_eTauRoI', 'L1_eTauxRoI']
                                                  ) )

    return result


def EfexMonitoringHistConfig(flags, eFexAlg):
    """
    Book the histograms for the efex monitoring. This is done in a separate method
    to the algorithm creation (above) because the histograms are based on the list of container keys
    given to the algorithm, which can be customized by the user before calling this method.
    """
    import math
    from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
    result = ComponentAccumulator()


    # make the athena monitoring helper again so we can add groups
    from TrigT1CaloMonitoring.LVL1CaloMonitoringConfig import L1CaloMonitorCfgHelper
    helper = L1CaloMonitorCfgHelper(flags,None,'EfexMonitoringCfg')
    helper.alg = eFexAlg

    # we don't add the algorithm again, use the supplied one
    EfexMonAlg = eFexAlg 
    baseGroupName = EfexMonAlg.PackageName

    # Some helpful variables for declaring the histograms
    # mainDir = 'L1Calo'
    trigPath = f'Developer/{eFexAlg.name}/' # Default Directory trigger path for output histos
    # Map from the key name to the output directory substructure.
    
    def pathFromKey(key,prefix="Nominal/"):
        path=prefix
        if "DAODSim" in key: path = "DAODSim/"
        elif "ReSim" in key: path = "ReSim/"
        elif "Sim" in key: path = "Sim/"
        if "_eEMx" in key: path += "eEMx"
        elif "_eEM" in key: path += "eEM"
        elif "_eTaux" in key: path += "eTAUx"
        elif "_eTau" in key: path += "eTAU"
        return path 
    
    cut_names = ["LowPtCut", "HiPtCut"] # List of cut names, for looping over to separate out histograms into directories
    cut_vals = [EfexMonAlg.LowPtCut, EfexMonAlg.HiPtCut] # List of values, for looping over to add to histogram titles

    # First, define the histograms with no Pt cut
    # add monitoring algorithm to group, with group name and main directory

    locIdxs = []
    for phiOct in range(0,8):
        for etaIdx in range(-25,25):
            locIdxs += [str(phiOct) + ":" + str(etaIdx)]

    for containerKey in (list(EfexMonAlg.eFexEMTobKeyList) + list(EfexMonAlg.eFexTauTobKeyList)):
        helper.defineHistogram(containerKey + '_nTOBs_nocut;h_n'+containerKey+'_nocut', title='Number of '+containerKey+';Number of '+containerKey+';Events',
                               fillGroup = baseGroupName,
                               path=trigPath+pathFromKey(containerKey)+"NoCut",
                               type='TH1I', xbins=100,xmin=-0.5,xmax=99.5)
        if "Sim" not in containerKey:
            # don't make these expensive plots for simulation
            helper.defineHistogram(f"LBN,{containerKey}_nTOBs_nocut;h_"+containerKey+"_nTOBs", title = "Average # of " + containerKey + " TOBs;LBN",
                                   fillGroup = baseGroupName + "_" + containerKey,
                                   path=trigPath+pathFromKey(containerKey)+"NoCut",
                                   type="TH2I",
                                   xbins=1,xmin=0,xmax=1,ybins=20,ymin=-0.5,ymax=19.5, opt=['kAddBinsDynamically'])


    helper.defineDQAlgorithm("Efex_eEM_etaThiMapFilled",
                             hanConfig={"libname":"libdqm_summaries.so","name":"Bins_Equal_Threshold","BinThreshold":"0."},
                             thresholdConfig={"NBins":[1,64*50]}, # currently there is 1 known deadspot in eEM, so that is allowed, anything else is a warning. Error if fully empty
                             )
    helper.defineDQAlgorithm("Efex_eTAU_etaThiMapFilled",
                             hanConfig={"libname":"libdqm_summaries.so","name":"Bins_Equal_Threshold","BinThreshold":"0."},
                             thresholdConfig={"NBins":[0,64*50]}, # everywhere should be filled, otherwise a warning (error if entirely empty)
                             )

    # Now define the histograms with low/hi Pt cut
    for cut_name, cut_val in zip(cut_names, cut_vals):
        cut_title_addition = '' if (cut_val == 0.0) else ' [Et>=' + '%.1f'%(cut_val/1000) + 'GeV]'
        # Em first
        for containerKey in EfexMonAlg.eFexEMTobKeyList:
            fillGroup = baseGroupName+'_'+containerKey+'_'+cut_name
            tobStr = containerKey
            # histograms of eEM variables
            helper.defineHistogram('nEMTOBs;h_nEmTOBs', title='Number of '+tobStr+'s'+cut_title_addition+';EM '+tobStr+'s;Number of EM '+tobStr+'s',
                                   fillGroup=fillGroup,
                                    type='TH1I', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=10,xmin=0,xmax=10)

            helper.defineHistogram('TOBTransverseEnergy;h_TOBTransverseEnergy', title=tobStr+' ET [MeV]'+cut_title_addition,
                                   fillGroup=fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=100,xmin=0,xmax=50000)

            helper.defineHistogram('TOBEta;h_TOBEta', title=tobStr+' Eta'+cut_title_addition,
                                   fillGroup=fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=50,xmin=-2.5,xmax=2.5)

            helper.defineHistogram('TOBPhi;h_TOBPhi', title=tobStr+' Phi'+cut_title_addition,
                                   fillGroup=fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=64,xmin=-math.pi,xmax=math.pi)

            helper.defineHistogram(f"TOBEta,TOBPhi;h_{containerKey}_{cut_name}_EtaPhiMap", title=tobStr+' Count'+cut_title_addition+';#eta;#phi',
                                   fillGroup=fillGroup,
                                   hanConfig={"display":"SetPalette(55)",
                                       "description":f"Inspect for hot/cold spots - check help for list of known hot/coldspots, then check <a href='./detail/h_{containerKey}_{cut_name}_posVsLBN'>detail timeseries</a>. Warning if more than 1 deadspot, but could just be low stats","algorithm":"Efex_eEM_etaThiMapFilled"},
                                    type='TH2F',
                                    path=(("Expert/Outputs/"+pathFromKey(containerKey,"")) if "Sim" not in containerKey and "x" not in containerKey else trigPath+pathFromKey(containerKey)+cut_name),
                                    xbins=50,xmin=-2.5,xmax=2.5,ybins=64,ymin=-math.pi,ymax=math.pi,opt=['kAlwaysCreate'])

            if "Sim" not in containerKey and "x" not in containerKey:
                helper.defineHistogram(f"LBN,binNumber;h_{containerKey}_{cut_name}_posVsLBN", title=tobStr+' Count'+cut_title_addition+';LB;50(y-1)+x',
                                   fillGroup=fillGroup,
                                   hanConfig={"description":f"Timeseries of TOB counts at each location ... y-axis relates to x and y bin numbers from <a href='../h_{containerKey}_{cut_name}_EtaPhiMap'>eta-phi map</a>. Use Projection X1 for 1D plot"},
                                   type='TH2I',
                                   path="Expert/Outputs/"+pathFromKey(containerKey,"")+"/detail",
                                   xbins=1,xmin=0,xmax=10,
                                   ybins=64*50,ymin=0.5,ymax=64*50+0.5,opt=['kAddBinsDynamically'])

            helper.defineHistogram('TOBshelfNumber;h_TOBshelfNumber', title=tobStr+' EM Shelf Number'+cut_title_addition,
                                   fillGroup=fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=2,xmin=0,xmax=2)

            helper.defineHistogram('TOBeFEXNumberSh0;h_TOBeFEXNumberShelf0', title=tobStr+' EM Module Number Shelf 0'+cut_title_addition,
                                   fillGroup=fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=12,xmin=0,xmax=12)

            helper.defineHistogram('TOBeFEXNumberSh1;h_TOBeFEXNumberShelf1', title=tobStr+' EM Module Number Shelf 1'+cut_title_addition,
                                   fillGroup=fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=12,xmin=0,xmax=12)

            helper.defineHistogram('TOBfpga;h_TOBfpga', title=tobStr+' EM FPGA'+cut_title_addition,
                                   fillGroup=fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=4,xmin=0,xmax=4)

            helper.defineHistogram('TOBReta;h_TOBReta', title=tobStr+' EM Reta'+cut_title_addition,
                                   fillGroup=fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name,xbins=250,xmin=0,xmax=1)

            helper.defineHistogram('TOBRhad;h_TOBRhad', title=tobStr+' EM Rhad'+cut_title_addition,
                                   fillGroup=fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=250,xmin=0,xmax=1)

            helper.defineHistogram('TOBWstot;h_TOBWstot', title=tobStr+' EM Wstot'+cut_title_addition,
                                   fillGroup=fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=250,xmin=0,xmax=1)

            threshold_labels = ['fail','loose','medium','tight']
            helper.defineHistogram('TOBReta_threshold;h_TOBReta_threshold', title=tobStr+' EM Reta threshold'+cut_title_addition,
                                   fillGroup=fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name,xbins=4,xmin=0,xmax=4.0,xlabels=threshold_labels)

            helper.defineHistogram('TOBRhad_threshold;h_TOBRhad_threshold', title=tobStr+' EM Rhad threshold'+cut_title_addition,
                                   fillGroup=fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=4,xmin=0,xmax=4.0,xlabels=threshold_labels)

            helper.defineHistogram('TOBWstot_threshold;h_TOBWstot_threshold', title=tobStr+' EM Wstot threshold'+cut_title_addition,
                                   fillGroup=fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=4,xmin=0,xmax=4.0,xlabels=threshold_labels)

        # Now Tau
        for containerKey in EfexMonAlg.eFexTauTobKeyList:
            fillGroup = baseGroupName+'_'+containerKey+'_'+cut_name
            tobStr = containerKey
            # plotting of eTau variables
            helper.defineHistogram('nTauTOBs;h_nTauTOBs', title='Number of '+tobStr+'s'+cut_title_addition+';Tau '+tobStr+'s;Number of Tau '+tobStr+'s',
                                    fillGroup = fillGroup,
                                    type='TH1I', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=10,xmin=0,xmax=10)

            helper.defineHistogram('tauTOBTransverseEnergy;h_tauTOBTransverseEnergy', title=tobStr+' Tau Transverse Energy [MeV]'+cut_title_addition,
                                    fillGroup = fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=100,xmin=0,xmax=50000)

            helper.defineHistogram('tauTOBEta;h_tauTOBEta', title=tobStr+' Tau Eta'+cut_title_addition,
                                    fillGroup = fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=60,xmin=-2.5,xmax=2.5)

            helper.defineHistogram('tauTOBPhi;h_tauTOBPhi', title=tobStr+' Tau Phi'+cut_title_addition,
                                    fillGroup = fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=100,xmin=-math.pi,xmax=math.pi)

            helper.defineHistogram(f"tauTOBEta,tauTOBPhi;h_{containerKey}_{cut_name}_EtaPhiMap", title='eTAU '+tobStr+' Count'+cut_title_addition+';#eta;#phi',
                                    fillGroup = fillGroup,
                                   hanConfig={"display":"SetPalette(55)",
                                              "description":f"Inspect for hot/cold spots - check help for list of known hot/coldspots, then check <a href='./detail/h_{containerKey}_{cut_name}_posVsLBN'>detail timeseries</a>. Warning if any deadspots/empty, but could just be low stats","algorithm":"Efex_eTAU_etaThiMapFilled"},
                                   type='TH2F',
                                   path=(("Expert/Outputs/"+pathFromKey(containerKey,"")) if "Sim" not in containerKey and "x" not in containerKey else (trigPath+pathFromKey(containerKey)+cut_name)),
                                   xbins=50,xmin=-2.5,xmax=2.5,ybins=64,ymin=-math.pi,ymax=math.pi,opt=['kAlwaysCreate'])

            if "Sim" not in containerKey and "x" not in containerKey:
                helper.defineHistogram(f"LBN,binNumber;h_{containerKey}_{cut_name}_posVsLBN", title='eTAU '+tobStr+' Count'+cut_title_addition+';LB;50(y-1)+x',
                               fillGroup=fillGroup,
                               hanConfig={"description":f"Timeseries of TOB counts at each location ... y-axis relates to x and y bin numbers from <a href='../h_{containerKey}_{cut_name}_EtaPhiMap'>eta-phi map</a>. Use Projection X1 for 1D plot"},
                               type='TH2I',
                               path="Expert/Outputs/"+pathFromKey(containerKey,"")+"/detail",
                               xbins=1,xmin=0,xmax=10,
                               ybins=64*50,ymin=0.5,ymax=64*50+0.5,opt=['kAddBinsDynamically'])

            helper.defineHistogram('tauTOBshelfNumber;h_tauTOBshelfNumber', title=tobStr+' Tau Shelf Number'+cut_title_addition,
                                    fillGroup = fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=2,xmin=0,xmax=2)

            helper.defineHistogram('tauTOBeFEXNumberSh0;h_tauTOBeFEXNumberShelf0', title=tobStr+' Tau Module Number Shelf 0'+cut_title_addition,
                                    fillGroup = fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=12,xmin=0,xmax=12)

            helper.defineHistogram('tauTOBeFEXNumberSh1;h_tauTOBeFEXNumberShelf1', title=tobStr+' Tau Module Number Shelf 1'+cut_title_addition,
                                    fillGroup = fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=12,xmin=0,xmax=12)


            helper.defineHistogram('tauTOBfpga;h_tauTOBfpga', title=tobStr+' Tau FPGA'+cut_title_addition,
                                    fillGroup = fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=4,xmin=0,xmax=4)

            helper.defineHistogram('tauTOBRcore;h_tauTOBRcore', title=tobStr+' Tau rCore'+cut_title_addition,
                                    fillGroup = fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=250,xmin=0,xmax=1)

            helper.defineHistogram('tauTOBRhad;h_tauTOBRhad', title=tobStr+' Tau rHad'+cut_title_addition,
                                    fillGroup = fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=250,xmin=0,xmax=1)

            helper.defineHistogram('tauTOBRcore_threshold;h_tauTOBRcore_threshold', title=tobStr+' Tau rCore threshold'+cut_title_addition,
                                    fillGroup = fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=4,xmin=0,xmax=4.0, xlabels=threshold_labels)

            helper.defineHistogram('tauTOBRhad_threshold;h_tauTOBRhad_threshold', title=tobStr+' Tau rHad threshold'+cut_title_addition,
                                    fillGroup = fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=4,xmin=0,xmax=4.0, xlabels=threshold_labels)

            helper.defineHistogram('tauTOBthree_threshold;h_tauTOBthree_threshold', title=tobStr+' Tau 3 taus threshold'+cut_title_addition,
                                    fillGroup = fillGroup,
                                    type='TH1F', path=trigPath+pathFromKey(containerKey)+cut_name, xbins=4,xmin=0,xmax=4.0, xlabels=threshold_labels)

    acc = helper.result()
    result.merge(acc)
    return result


if __name__=='__main__':
    # set input file and config options
    from AthenaConfiguration.AllConfigFlags import initConfigFlags
    import glob

    # MCs processed adding L1_eEMRoI
    inputs = glob.glob('/eos/user/t/thompson/ATLAS/LVL1_mon/MC_ESD/l1calo.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.eFex_gFex_2022-01-13T2101.root')

    flags = initConfigFlags()
    flags.Input.Files = inputs
    flags.Output.HISTFileName = 'ExampleMonitorOutput_LVL1_MC.root'

    flags.lock()
    flags.dump() # print all the configs

    from AthenaConfiguration.MainServicesConfig import MainServicesCfg  
    from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
    cfg = MainServicesCfg(flags)
    cfg.merge(PoolReadCfg(flags))

    EfexMonitorCfg = EfexMonitoringConfig(flags)
    cfg.merge(EfexMonitorCfg)

    # options - print all details of algorithms, very short summary 
    cfg.printConfig(withDetails=False, summariseProps = True)

    nevents=10
    cfg.run(nevents)

